= Hacking the M-star

Current TLDR Status: WIP -- not for general consumption -- no
validation, missing software, very active project as of 8/2021.

We encourage tinkering and experimentation. This is a guide to
reading the repository.

== Touring the repository

The web/ subdirectory is documentation and web resources.  The documentation
is written in asciidoc; it is automatically formatted and deployed to the public
M-star website on every ropository push.

The QMK/ directory, if present, is QMK firmware to run on M-Star
controllers that has not yet been pushed to the public QMK
repository.  See the https://qmk.fm/[QMK website] for more about that
software.

The hardware/ directory describes, unsurprisingly, the hardware.
Under it you will find:

README.adoc::
	Designer's notes on the hardware.  Read before
	tinkering.

STM32::
	A copy of the KiCad library gor designing aroundf the STM32
	microcontroller that M-Star projects use.

common::
	The common/ directory contains footprints for the IBM-specific pin
	connectors - "triomate style" single-sided FFC connectors.  These custom
	models include silkscreen hints to help you get the orientation
	correct.

gerber::
	Plot files you can upload to a PCB fabrication house to get a
	board. Kicad can produce them from the master file in the
	repo, but by convention most people version the Gerbers as
	well. The reason for this is that different versions of KiCad
	may produce different output in a years long timescale.

classic.*, mini.*, unidapt.*::
	Groups of files describing individual hardware products.
	Each one of thesese is interpreted as a project by KiCad.

fp-lib-table, sym-lib-table, mini-cache.lib::
	Internal files that tell KiCad where to find the STM32 and
	common libraries.

== KiCad Notes:

You will need the STM32 Library (part of this repo) and the custom
footprint/components for the IBM FFC 4-8-12-16-20 pin connectors. The
other components *should* be in the standard KiCad library. Symbol
Editor > Add Library > STM32 folder > Symbols Folder >
YAAJ_BlackPill_SWD_Breakout.

The following notes will make the most sense if you are looking at the
schematic or board files.

In supporting so many M and M-alike keyboards there are a fair number
of variants to consider.

The connectors change a bit over time and how the LEDs are handled
varies a fair bit.  The location of the SDL Jack is also not
consistent (There is a lot more info on the project website -- read
about it there).

This PCB has two possible placements for USB type B jacks depending on
your SDL jack needs. Center-Exit is on the right side of the PCB and
the left ext is on the left side of the PCB. (Don't solder both,
obviously...).

The USB B connector is recessed to reduce mechanical tension on the PCB. 

Each USB B connection has 3 SMT components R1,2,5 for the USB_B -
Alternate mounting and R16, R3, R4 for the primary USB mounting. The
idea is that you mount low-resistance resistors (0-22 ohm) or solder
bridges depending on which USB connection you actually plan to use.

The "Bluepill" is designed to be soldered directly to the circuit
board with standard 0.100" pin headers. A pin+socket won't work due to
limited vertical clearance inside a model M.

The "Bluepill" carrier has almost all the passive components you need,
which greatly reduces the number of passives needed on this PCB. Only
the Red/Green LEDs and the mini-USB jack on the "Bluepill" are the
only components on the carrier not actually used.

Depending on your bluepill you may have solder holes or right-angle
pin headers pre-installed on the 4 pins: SWDIO, SWDCLK, 3.3v, GND (for
STM programming). We only re-use *only* SWDIO for a GPIO for Row 11 of
the keyboard input mtatrix. Row 11 should only be used for Terminal
122s or any M variants that need 16+12+4 cable combos. (Most people
will not need row 11 anyway).

== M-Star Classic

=== v002 Board Notes 

The aim is to document (see website) variants of "good" Model M and
M-derivative keyboards, their various controllers, and hopefully offer
an PCB replacement controller that's actually not terrible.

We believe this PCB, which is a glorified carrier for the <$5 US
"BluePill" STM32 DIN package, will support M122, M101/102, SSK and a
few other variants. We believe the "AT" M122 is also supported.

=== Supported IBM model numbers 

See the https://esr.gitlab.io/m-star/classic.html[Classic front page]

Note that there are currently no plans to support trackpoint functions such as with the 13H6705 

TODO::

* Can the board layout or soldering be simplified to cover more variants?
* 3d Printable shim to fill the gaps around the USB area and provide mechanical strain relief for the PCB USB B connector
* Actual Testing
* QMK Layouts.... (e.g. "The Firmware") 

=== The Classic cannot support:

2021 Unicomp Mini-M - It has a 16+16 (16+12+4 LEDs) Matrix configuration and,
well, that's a pain to cable in. Also the connectors are rotated 180
degrees (!?)

There are, however, some alternate PCB layouts and schematics for the
Mini-M (different PCB for interfacing the 16+16 matrix FFC found in
a Minm-M) in this repo. THEY ARE UNTESTED. Watch this project for
update.

"Unidapt" The 2020/2021 Press-Fit (with screws) Unicomp Model M is
also as-yet currently unsupported. There is a "Press-Fit adapter"
which is an attempt at making an adapter that'll bring out the PCB FFC
connector. It is currently UNTESTED.

A custom footprint for the Unicomp-style FFC PCB gold-finger connector
was also added to this repo. It is currently UNTESTED.

=== LEDs on the M (throughout time and space)

LEDs on Ms are tricky in terms of what you need to know if you're
modernizing the controller. The manufacturing history of the M seems
to have been "Eh, whatever LEDs we can get. Cheap!" so the M-Star Classic
needs to support any random thing for LEDs. And cable placement.

In order to do that we have to support:

4 wires for LEDs in the FFC 12-pin pin header, two placements of FFC
4-pin cables and a Molex "hard-wired" LED board.  This is mainly J4
and J5 on the PCB, but the last 4 pins of J4 are configurable between
matrix pins or LED pins.

=== Jumpers 

There are 4 jumpers on the board JP1, 2, 3 and 4. JP4 controlls the
pull-up voltage value sent to the LEDs: 3.3v or 5v. This voltage is
fed to J3, J4 and J5 depending on what you do with the solder pads.

J4/5 and J3 each has its own set of solder pads for resistors you
might need. For example, you might need 220ohm pull-up resistors for
that super cherry 1397000 while a later 13H6705 actually uses 3.3v
LEDs with much higher resistors.

TODO::
  For the project team is to work out some suggested resistance
  values and add them to the table on the "Logical and physical
  interfaces" page on the website.

JP4 has an extra pin, J9, for "Row 11" from SWDIO to make it easy to
solder in a bodge wire should you be going for a 16+12+4 config where
the 16 and 12 are rows and columns in the matrix, and 4 is LEDs (there
are enough pins! with 1 left over! on the "BluePill").

Additionally J6 breaks out all the LEDs, 3.3v and 5v to solder pads so
you can DIY it, if you want. Want to convert an M122 or M101/102
terminal keyboard to USB (those don't have LEDs) and just solder your
own LEDs? Sure then this is for you.

The "BluePill" is a 3.3v critter, but the LED pins are routed to +5v
safe pins, so it is possible to pull the LED lines all the way up to
+5v from the USB if needed.

=== J2 and J3 notes:

So the IBM FFC cable connector is pretty common. The PCB is designed
to accomodate a lot of variants so you might be putting a smaller
connector in to the PCB than is laid out on the PCB. There are helpful
markers on the silkscreen to guide you.

(Nearly?) All M122 keyboards, including the aforementioned cherry AT
122, have a 20 pin FFC as the primary matrix layer. Most normal
101/102 Model Ms just have 16.

So you'd solder in a 16 pin FFC connector into J2 ("J2 Columns" on the
PCB) starting at pin 4 (see marker on the silkscreen). Pin 1 is the
right most pin and is labeled "Row 8" on the schematic because that
pin could be a column at the startof J2 or a row if it's used at the
end of the second FFC connector ("Rows").

Even though this is a 20 pin connector 'most' people will use it as a
16 pin connector.

The physical alignment here is also important as we tried to take into
account the physical placement of the FFC cables in offering this
PCB. It's tricky.

The spacing between the 16 (or 20) "Columns" ribbon and the 8 or 12
(or 16) "Rows" ribbon does vary a bit.

Similarly with "J3 Rows" most people will use either an 8 or 12 pin
ribbon there. 8 pins if you have another 4 pins for your LEDs or 12 if
your LED wires are built-in. There is a hint on the silkscreen if you
are soldering an 8 pin header into the 12 pins available on this
socket.

Again, the physical alignment of the 8 pin socket should work out for
you, even though a larger connector would work.

For the LEDs, we know  there are 3 physical connector variants with 4
possible physical placements:

* LED ribbon as part of the "Rows" cable (last 4 pins)
* LED ribbon stand-alone for left-cable-exit models
* LED ribbon stand-alone for center-cable-exit models
* LEDs wired into physical PCB. 

LEDs may have current-limiting resistors and may need to be pulled up
to 3.3v or 5v.

TODO::
   We are working on documenting which one you need for what
   model. For now in the project lifecycle your skill level must be
   sufficient that the words up till now are making sense to you. But we
   are working on making it as easy as possible.

// end


