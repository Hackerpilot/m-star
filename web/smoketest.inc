Before you open your keyboard case is a good time for what engineers
call a "smoke test" on your M-Star controller.

Start by sitting your M-Star Classic on your insulating pad.  Holding
it in a hand will work for this smoke test, but you'll need the pad
for later testing. What it's for is to electrically isolate your
controller, which can get confused if the surface you put it on
allows conductive or capacitive paths between parts of the
controller's underside.

Plug your USB cable into both the USB jack on the controller and a
computer.  You should see a red light go on.

If you don't see that red light, you have a bad cable or a bad port
or (just possibly) a dead M-Star. Swap cables and ports to check.

Once you've seen the red light come on, unplug the M-Star
so it's not receiving power during the next several steps.
