= M-Star Classic Keyboard Controller

THESE PAGES ARE UNDER CONSTRUCTION.  THE HARDWARE IS NOT YET SHIPPING.

The M-Star Classic is a replacement controller board for IBM, Lexmark, and
MaxiSwitch Model M keyboards made between 1985 and 1995.  Some Model
Ms made up until 1999 are also compatible.

== Features

Here's what installing a M-Star Classic controller should get you:

* Native, high-quality USB support - no more adapters to misconfigure or lose.

* Runs QMK, so you can have remapping and macros.

* Dramatically reduces your Model M's power requirements.

// Stock Ms with no adapters draw 100-120mAh.
// Wendell's design with the Blue Pill clocked down to 30MHz should draw around 30mAh,
// That's about a factor of three improvement over no adapters.

There are USB problems arising from high power draw and flaky USB
converters that this upgrade is likely (but not certain) to fix:

* Your Model M is not being recognized after system boot, requiring it
  to be unplugged and replugged.

* When you are in your computer's BIOS configuration mode (as opposed
  to running your normal operating system) it doesn't take input from
  your Model M.

* Your Model M doesn't work through cheap USB hubs.

We say it's not certain because these problems may actually be due to
dodgy USB hardware or cables, or even defective USB software stacks,
downstream from your keyboard and whatever converter(s) you were using.
QMK is pretty good at adapting to slightly substandard USB
implementations, but there's only so much it can do about the more
broken ones.

Other than speaking USB natively and drawing less power, your keyboard
should work exactly as it did before, sending the same input events
for each keystroke, unless and until you cusTomizew it via QMK.

== Which Model Ms you can upgrade

Flip over your M and look for a manufacturing label on the bottom.  If
it says "Unicomp", you cannot use a M-Star Classic controller.  It must say
"IBM", "Lexmark", or "MaxiSwitch".

Also look at the date.  Many Lexmarks and IBMs made in 1995 and later
can't be upgraded with a M-Star Classic either, as the controller board was
shrunk and moved to just under the lock light panel a year before the
Unicomp buyout. The location of the cable exit hole is an external
sign of this.

Your M must either have a female SDL jack or an exit hole for the cable
near the center rear of the case. If it has a cable exit hole way
over on the right hand side, you have one of the smaller late
controller boards and can't use a M-Star Classic.

On M variants with a trackpoint or trackball, such as the MaxiSwitch
M13, we don't support those input devices.

== What we provide

If you already have a M-Star Classic, here are the link:classic-installation.html[installation instructions].

You can read about link:classic-customizing.html[customizing your keyboard].
The most common reason to do this is to work around the M's lack of a
dedicated Super (or Windows) key.

You can read the Classic's link:classic-mission.html[mission statement].

There have been a number of previous attempts at fielding a
replacement Model M controller.  You can
link:classic-previous-attempts.html[study those], which is recommended if you
aim to contribute.

We've done a lot of knowledge capture about the
physical and logical interfaces of the Model M.
These inform our controller design, but we've taken care
to also produce a link:interfaces.html[ground-truth document]
so that any future attempts to do what we're doing can be
even better through not having to start from zero.

The M-Star Classic technology package is open source. We encourage tinkering.
You can get all the CAD files and documentation by cloning the
https://gitlab.com/esr/m-star[public repository].

// end
