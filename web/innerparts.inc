plate assembly::
   The larger of just two parts inside the case. A curved metal
   *backplate* supports sensor membranes and switches, and on top of
   those is a curved plastic *barrel plate*.  The plates are joined
   together by plastic rivets and form a unit which you will *not*
   have to disassemble or modify.

PCB::
   Generically: Printed circuit board.  Specifically, the
   factory-installed controller electronics in your Model M - the
   smaller of two parts inside the case.  You're going to replace this
   with an M-Star controller, which is also generically a PCB but a
   different one.

ribbon cables::
   These connect the plate assembly to the PCB. So called because they look like
   broad gray plastic ribbons.  In the trade they're often called
   "FFCs" - Flat Flexible Connectors.

